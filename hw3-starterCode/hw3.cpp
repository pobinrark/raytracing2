/* **************************
 * CSCI 420
 * Assignment 3 Raytracer
 * Name: <Your name here>
 * *************************
*/
//comment by mrpark
#ifdef WIN32
  #include <windows.h>
#endif

#if defined(WIN32) || defined(linux)
  #include <GL/gl.h>
  #include <GL/glut.h>
#elif defined(__APPLE__)
  #include <OpenGL/gl.h>
  #include <GLUT/glut.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Vector3.h"
#ifdef WIN32
  #define strcasecmp _stricmp
#endif

#include <imageIO.h>

#define MAX_TRIANGLES 20000
#define MAX_SPHERES 100
#define MAX_LIGHTS 100

char * filename = NULL;

//different display modes
#define MODE_DISPLAY 1
#define MODE_JPEG 2

int mode = MODE_DISPLAY;

//you may want to make these smaller for debugging purposes
#define WIDTH 640
#define HEIGHT 480

//the field of view of the camera
#define fov 60.0
#define PI 3.1415926

unsigned char buffer[HEIGHT][WIDTH][3];

struct Vertex
{
  double position[3];
  double color_diffuse[3];
  double color_specular[3];
  double normal[3];
  double shininess;
};

struct Triangle
{
  Vertex v[3];
};

struct Sphere
{
  double position[3];
  double color_diffuse[3];
  double color_specular[3];
  double shininess;
  double radius;
};

struct Light
{
  double position[3];
  double color[3];
};

struct Ray
{
	Vector3 pos;
	Vector3 dir;
};

struct Ray ComputeCameraRay(int x, int y)
{
	struct Ray newRay;
	double aspect = 1.3333;
	double screenx = -aspect*tan((PI / 3) / 2);
	double x_inc = -(screenx / 640.0) * 2;
	double screeny = -tan((PI / 3) / 2);
	double y_inc = -(screeny / 480.0) * 2;

	newRay.pos = Vector3(0, 0, 0);

	Vector3 direction = Vector3(screenx + x_inc*x, screeny + y_inc*y, -1);
	direction.normalize();
	newRay.dir = direction;

	return newRay;
}

Triangle triangles[MAX_TRIANGLES];
Sphere spheres[MAX_SPHERES];
Light lights[MAX_LIGHTS];
double ambient_light[3];

int num_triangles = 0;
int num_spheres = 0;
int num_lights = 0;

void plot_pixel_display(int x,int y,unsigned char r,unsigned char g,unsigned char b);
void plot_pixel_jpeg(int x,int y,unsigned char r,unsigned char g,unsigned char b);
void plot_pixel(int x,int y,unsigned char r,unsigned char g,unsigned char b);

double clamp(double val, double max, double min)
{
	if (val > max){
		return max;
	}
	if (val < min){
		return min;
	}
	return val;
}

bool didHitTriangle(Ray r, double& out, Triangle& tOut)
{
	bool hitAny = false; //adding random comment
	double minimumT = 100000;
	Triangle hitTriangle;
	Triangle t;
	if (num_triangles <= 0)
	{
		return false;
	}
	for (int i = 0; i<num_triangles; i++)
	{
		t = triangles[i];
		Vector3 P1, P2, P3; //triangle points
		P1 = Vector3(t.v[0].position[0], t.v[0].position[1], t.v[0].position[2]);
		P2 = Vector3(t.v[1].position[0], t.v[1].position[1], t.v[1].position[2]);
		P3 = Vector3(t.v[2].position[0], t.v[2].position[1], t.v[2].position[2]);

		//find edges
		Vector3 edge1 = P2 - P1;
		Vector3 edge2 = P3 - P1;

		Vector3 pvec = cross(r.dir, edge2);

		double det = dot(edge1, pvec);

		if (det < 0.000001 && det > -0.000001)
		{
			continue;
		}
		double inv_det = 1 / det;

		Vector3 tvec = r.pos - P1;
		double u = dot(tvec, pvec) * inv_det;

		if (u < 0 || u > 1){
			continue;
		}

		Vector3 qvec = cross(tvec, edge1);

		double v = dot(r.dir, qvec) * inv_det;

		if (v < 0 || u + v > 1){
			continue;
		}

		double t_0 = dot(edge2, qvec) * inv_det;

		if (t_0 > 0.000001)
		{
			if (t_0 < minimumT)
			{
				minimumT = t_0;
				tOut = t;
				hitAny = true;
			}
		}
	}
	out = minimumT;
	//tOut = t;
	return hitAny;
}

bool didHitCircle(Ray r, double& out, Sphere& sOut)
{
	bool hitAny = false;
	double minimumT = 1000000;
	Sphere hitSphere;
	Sphere s;
	if (num_spheres <= 0)
	{
		return false;
	}
	for (int i = 0; i<num_spheres; i++)
	{
		s = spheres[i];
		Vector3 center = Vector3(s.position[0], s.position[1], s.position[2]);
		double radius = s.radius;

		double b = 2 * (r.dir.x() * (r.pos.x() - center.x()) +
			r.dir.y() * (r.pos.y() - center.y()) +
			r.dir.z() * (r.pos.z() - center.z()));

		double c = (r.pos.x() - center.x()) * (r.pos.x() - center.x()) +
			(r.pos.y() - center.y()) * (r.pos.y() - center.y()) +
			(r.pos.z() - center.z()) * (r.pos.z() - center.z()) -
			radius * radius;

		double determinant = b * b - 4 * c;
		if (determinant < 0.0001)
		{
			continue;
		}

		double t_0 = (-b + sqrt(determinant)) / 2;
		double t_1 = (-b - sqrt(determinant)) / 2;

		if (t_0 < 0.00001 && t_1 < 0.00001)
		{
			continue;
		}

		if (t_0 >= 0.0001 && t_1 >= 0.0001)
		{
			double minT = fmin(t_0, t_1);
			hitAny = true;
			if (minT < minimumT)
			{
				minimumT = minT;
				sOut = s;
			}
		}
		else
		{
			double minT = fmax(t_0, t_1);
			hitAny = true;
			if (minT < minimumT)
			{
				minimumT = minT;
				sOut = s;
			}
		}

	}
	//sOut = s;
	out = minimumT;
	return hitAny;
}


Vector3 AddAmbient(Vector3 color)//another random comment
{
	Vector3 newColor = Vector3(clamp(color.x() + ambient_light[0] * 255, 255, 0),
		clamp(color.y() + ambient_light[1] * 255, 255, 0),
		clamp(color.z() + ambient_light[2] * 255, 255, 0));
	return newColor;

}


Vector3 colorSphere(Light l, Vector3 color, Vector3 pos, Sphere s)//second commit
{
	//find the normal of the intersection point on the sphere based on its center
	Vector3 center = Vector3(s.position[0], s.position[1], s.position[2]);
	Vector3 sNorm = pos - center;
	sNorm = (1 / s.radius) * sNorm;
	sNorm.normalize();

	//Find the vector to the light source
	Vector3 lightLoc = Vector3(l.position[0], l.position[1], l.position[2]);
	Vector3 lightNorm = lightLoc - pos;
	lightNorm.normalize();

	Vector3 viewDir = pos;
	viewDir.normalize();

	Vector3 reflect = lightNorm - 2.0 * dot(lightNorm, sNorm) * sNorm;
	reflect.normalize();

	double vDotr = fmax(dot(viewDir, reflect), 0);

	Vector3 newColor = Vector3(clamp(color.x() + (s.color_diffuse[0] * 255.0 * dot(lightNorm, sNorm) + s.color_specular[0] * 255.0 * pow(vDotr, s.shininess)), 255, 0),
		clamp(color.y() + (s.color_diffuse[1] * 255.0 * dot(lightNorm, sNorm) + s.color_specular[1] * 255.0 * pow(vDotr, s.shininess)), 255, 0),
		clamp(color.z() + (s.color_diffuse[2] * 255.0 * dot(lightNorm, sNorm) + s.color_specular[2] * 255.0 * pow(vDotr, s.shininess)), 255, 0));
	return newColor;

}



Vector3 colorTriangle(Light l, Vector3 color, Vector3 pos, Triangle t, Ray r)
{
	Vector3 P1, P2, P3; //triangle points
	P1 = Vector3(t.v[0].position[0], t.v[0].position[1], t.v[0].position[2]);
	P2 = Vector3(t.v[1].position[0], t.v[1].position[1], t.v[1].position[2]);
	P3 = Vector3(t.v[2].position[0], t.v[2].position[1], t.v[2].position[2]);

	//find edges
	Vector3 edge1 = P2 - P1;
	Vector3 edge2 = P3 - P1;

	Vector3 pvec = cross(r.dir, edge2);

	double det = dot(edge1, pvec);

	double inv_det = 1 / det;

	Vector3 tvec = r.pos - P1;
	double u = dot(tvec, pvec) * inv_det;


	Vector3 qvec = cross(tvec, edge1);

	double v = dot(r.dir, qvec) * inv_det;

	Vector3 P1Normal = Vector3(t.v[0].normal[0], t.v[0].normal[1], t.v[0].normal[2]);
	P1Normal.normalize();
	Vector3 P2Normal = Vector3(t.v[1].normal[0], t.v[1].normal[1], t.v[1].normal[2]);
	P2Normal.normalize();
	Vector3 P3Normal = Vector3(t.v[2].normal[0], t.v[2].normal[1], t.v[2].normal[2]);
	P3Normal.normalize();

	Vector3 triNormal = ((1 - u - v) * P1Normal) + (u * P2Normal) + (v * P3Normal);
	triNormal.normalize();

	//Find the vector to the light source
	Vector3 lightLoc = Vector3(l.position[0], l.position[1], l.position[2]);
	Vector3 lightNorm = lightLoc - pos;
	lightNorm.normalize();

	Vector3 viewDir = pos;
	viewDir.normalize();

	//compute specular
	Vector3 spec0 = Vector3(t.v[0].color_specular[0], t.v[0].color_specular[1], t.v[0].color_specular[2]);
	Vector3 spec1 = Vector3(t.v[1].color_specular[0], t.v[1].color_specular[1], t.v[1].color_specular[2]);
	Vector3 spec2 = Vector3(t.v[2].color_specular[0], t.v[2].color_specular[1], t.v[2].color_specular[2]);

	Vector3 specular = (1.f - u - v) * spec0 + u * spec1 + v * spec2;

	//compute diffuse
	Vector3 diff0 = Vector3(t.v[0].color_diffuse[0], t.v[0].color_diffuse[1], t.v[0].color_diffuse[2]);
	Vector3 diff1 = Vector3(t.v[1].color_diffuse[0], t.v[1].color_diffuse[1], t.v[1].color_diffuse[2]);
	Vector3 diff2 = Vector3(t.v[2].color_diffuse[0], t.v[2].color_diffuse[1], t.v[2].color_diffuse[2]);

	Vector3 diffuse = (1.f - u - v) * diff0 + u * diff1 + v * diff2;

	//compute shine
	double shine0 = t.v[0].shininess;
	double shine1 = t.v[1].shininess;
	double shine2 = t.v[2].shininess;

	double alpha = (1.f - u - v) * shine0 + u * shine1 + v * shine2;

	Vector3 L = Vector3(l.color[0], l.color[1], l.color[2]);

	double lightDotNorm = fmax(dot(lightNorm, triNormal), 0);


	Vector3 reflect = lightNorm - 2.0 * lightDotNorm * triNormal;
	reflect.normalize();


	double vDotr = fmax(dot(viewDir, reflect), 0);


	Vector3 temp = (diffuse * (lightDotNorm)+specular * pow(vDotr, alpha));

	Vector3 temp2 = Vector3(temp.x() * L.x(), temp.y() * L.y(), temp.z() * L.z());

	Vector3 newColor = Vector3(clamp(color.x() + (temp2.x() * 255), 255, 0),
		clamp(color.y() + (temp2.y() * 255), 255, 0),
		clamp(color.z() + (temp2.z() * 255), 255, 0));
	return newColor;

}

bool isInShadow(Light l, Vector3 pos)
{
	Vector3 lightLoc = Vector3(l.position[0], l.position[1], l.position[2]);
	Vector3 lightDir = lightLoc - pos;
	lightDir.normalize();

	Ray shadowRay;
	shadowRay.pos = pos;
	shadowRay.dir = lightDir;

	double tDist;
	Sphere sHit;
	Triangle tHit; //these values are useless, I just need something to put into the function

	bool collision = false;
	Vector3 furthestIntersect = Vector3(0, 0, 0);

	if (didHitCircle(shadowRay, tDist, sHit))
	{
		if (tDist > 0.001){
			//check distances to make sure the shadows are being done correctly
			double hitToLight = (lightLoc - Vector3(shadowRay.pos.x() + (shadowRay.dir.x() * tDist),
				shadowRay.pos.y() + (shadowRay.dir.y() * tDist),
				shadowRay.pos.z() + (shadowRay.dir.z() * tDist))).abs();
			double posToLight = (lightLoc - pos).abs();
			if (hitToLight < posToLight)
				collision = true;
		}
	}

	if (didHitTriangle(shadowRay, tDist, tHit))
	{
		if (tDist > 0.001){
			//check distances to make sure the shadows are being done correctly
			double hitToLight = (lightLoc - Vector3(shadowRay.pos.x() + (shadowRay.dir.x() * tDist),
				shadowRay.pos.y() + (shadowRay.dir.y() * tDist),
				shadowRay.pos.z() + (shadowRay.dir.z() * tDist))).abs();
			double posToLight = (lightLoc - pos).abs();
			if (hitToLight < posToLight)
				collision = true;
		}
	}

	if (collision && (furthestIntersect - pos).abs() > 0.5){
		return true;
	}


	return false;
}
//MODIFY THIS FUNCTION
void draw_scene()
{
	unsigned int x, y;
	Vector3 CamPos = Vector3(0, 0, 0);
	Vector3 lookAt = Vector3(0, 0, -1);
	Vector3 upVec = Vector3(0, 1, 0);
	float aspect = 1.3333;


	//simple output
	for (x = 0; x<WIDTH; x++)
	{
		glPointSize(2.0);
		glBegin(GL_POINTS);
		for (y = 0; y < HEIGHT; y++)
		{
			//We are starting at (0,0) in the screen, shoot a ray out.
			Ray trace = ComputeCameraRay(x, y);
			double triIntersection = 1000000;
			double sphereIntersection = 1000000;
			Triangle InterstectedTri;
			Sphere IntersectedSphere;

			bool didHitTri = didHitTriangle(trace, triIntersection, InterstectedTri);
			bool didHitSphere = didHitCircle(trace, sphereIntersection, IntersectedSphere);

			if (didHitTri || didHitSphere){

				if ((didHitTri && didHitSphere) && triIntersection < sphereIntersection)
				{
					//we hit the triangle first
					//check if it is in shadow
					Vector3 triangleColor = Vector3();
					Vector3 hitPos = Vector3(trace.pos.x() + trace.dir.x() * triIntersection, trace.pos.y() + trace.dir.y() * triIntersection, trace.pos.z() + trace.dir.z() * triIntersection);
					for (int i = 0; i<num_lights; i++)
					{
						if (!isInShadow(lights[i], hitPos))
						{
							//plot_pixel(x,y,256,10,50);
							triangleColor = colorTriangle(lights[i], triangleColor, hitPos, InterstectedTri, trace);
						}
					}
					triangleColor = AddAmbient(triangleColor);
					plot_pixel(x, y, triangleColor.x(), triangleColor.y(), triangleColor.z());


				}
				else if ((didHitTri && didHitSphere) && triIntersection > sphereIntersection)
				{
					//we hit the sphere first
					Vector3 sphereColor = Vector3();
					for (int i = 0; i<num_lights; i++)
					{
						Vector3 hitPos = Vector3(trace.pos.x() + trace.dir.x() * sphereIntersection, trace.pos.y() + trace.dir.y() * sphereIntersection, trace.pos.z() + trace.dir.z() * sphereIntersection);

						if (!isInShadow(lights[i], hitPos))
						{
							sphereColor = colorSphere(lights[i], sphereColor, hitPos, IntersectedSphere);
						}
					}

					sphereColor = AddAmbient(sphereColor);
					plot_pixel(x, y, sphereColor.x(), sphereColor.y(), sphereColor.z());
				}
				else if (didHitTri)
				{
					Vector3 triangleColor = Vector3();
					Vector3 hitPos = Vector3(trace.pos.x() + trace.dir.x() * triIntersection, trace.pos.y() + trace.dir.y() * triIntersection, trace.pos.z() + trace.dir.z() * triIntersection);
					for (int i = 0; i<num_lights; i++)
					{
						if (!isInShadow(lights[i], hitPos))
						{
							//plot_pixel(x,y,256,10,50);
							triangleColor = colorTriangle(lights[i], triangleColor, hitPos, InterstectedTri, trace);
						}
					}
					triangleColor = AddAmbient(triangleColor);
					plot_pixel(x, y, triangleColor.x(), triangleColor.y(), triangleColor.z());
				}
				else //didHitSphere
				{
					//we hit the sphere first
					Vector3 sphereColor = Vector3();
					for (int i = 0; i<num_lights; i++)
					{
						Vector3 hitPos = Vector3(trace.pos.x() + trace.dir.x() * sphereIntersection, trace.pos.y() + trace.dir.y() * sphereIntersection, trace.pos.z() + trace.dir.z() * sphereIntersection);

						if (!isInShadow(lights[i], hitPos))
						{
							sphereColor = colorSphere(lights[i], sphereColor, hitPos, IntersectedSphere);
						}
					}

					sphereColor = AddAmbient(sphereColor);
					plot_pixel(x, y, sphereColor.x(), sphereColor.y(), sphereColor.z());

				}
			}


		}
		glEnd();
		glFlush();
	}
	printf("Done!\n"); fflush(stdout);
}

void plot_pixel_display(int x, int y, unsigned char r, unsigned char g, unsigned char b)
{
  glColor3f(((float)r) / 255.0f, ((float)g) / 255.0f, ((float)b) / 255.0f);
  glVertex2i(x,y);
}

void plot_pixel_jpeg(int x, int y, unsigned char r, unsigned char g, unsigned char b)
{
  buffer[y][x][0] = r;
  buffer[y][x][1] = g;
  buffer[y][x][2] = b;
}

void plot_pixel(int x, int y, unsigned char r, unsigned char g, unsigned char b)
{
  plot_pixel_display(x,y,r,g,b);
  if(mode == MODE_JPEG)
    plot_pixel_jpeg(x,y,r,g,b);
}

void save_jpg()
{
  printf("Saving JPEG file: %s\n", filename);

  ImageIO img(WIDTH, HEIGHT, 3, &buffer[0][0][0]);
  if (img.save(filename, ImageIO::FORMAT_JPEG) != ImageIO::OK)
    printf("Error in Saving\n");
  else 
    printf("File saved Successfully\n");
}

void parse_check(const char *expected, char *found)
{
  if(strcasecmp(expected,found))
  {
    printf("Expected '%s ' found '%s '\n", expected, found);
    printf("Parse error, abnormal abortion\n");
    exit(0);
  }
}

void parse_doubles(FILE* file, const char *check, double p[3])
{
  char str[100];
  fscanf(file,"%s",str);
  parse_check(check,str);
  fscanf(file,"%lf %lf %lf",&p[0],&p[1],&p[2]);
  printf("%s %lf %lf %lf\n",check,p[0],p[1],p[2]);
}

void parse_rad(FILE *file, double *r)
{
  char str[100];
  fscanf(file,"%s",str);
  parse_check("rad:",str);
  fscanf(file,"%lf",r);
  printf("rad: %f\n",*r);
}

void parse_shi(FILE *file, double *shi)
{
  char s[100];
  fscanf(file,"%s",s);
  parse_check("shi:",s);
  fscanf(file,"%lf",shi);
  printf("shi: %f\n",*shi);
}

int loadScene(char *argv)
{
  FILE * file = fopen(argv,"r");
  int number_of_objects;
  char type[50];
  Triangle t;
  Sphere s;
  Light l;
  fscanf(file,"%i", &number_of_objects);

  printf("number of objects: %i\n",number_of_objects);

  parse_doubles(file,"amb:",ambient_light);

  for(int i=0; i<number_of_objects; i++)
  {
    fscanf(file,"%s\n",type);
    printf("%s\n",type);
    if(strcasecmp(type,"triangle")==0)
    {
      printf("found triangle\n");
      for(int j=0;j < 3;j++)
      {
        parse_doubles(file,"pos:",t.v[j].position);
        parse_doubles(file,"nor:",t.v[j].normal);
        parse_doubles(file,"dif:",t.v[j].color_diffuse);
        parse_doubles(file,"spe:",t.v[j].color_specular);
        parse_shi(file,&t.v[j].shininess);
      }

      if(num_triangles == MAX_TRIANGLES)
      {
        printf("too many triangles, you should increase MAX_TRIANGLES!\n");
        exit(0);
      }
      triangles[num_triangles++] = t;
    }
    else if(strcasecmp(type,"sphere")==0)
    {
      printf("found sphere\n");

      parse_doubles(file,"pos:",s.position);
      parse_rad(file,&s.radius);
      parse_doubles(file,"dif:",s.color_diffuse);
      parse_doubles(file,"spe:",s.color_specular);
      parse_shi(file,&s.shininess);

      if(num_spheres == MAX_SPHERES)
      {
        printf("too many spheres, you should increase MAX_SPHERES!\n");
        exit(0);
      }
      spheres[num_spheres++] = s;
    }
    else if(strcasecmp(type,"light")==0)
    {
      printf("found light\n");
      parse_doubles(file,"pos:",l.position);
      parse_doubles(file,"col:",l.color);

      if(num_lights == MAX_LIGHTS)
      {
        printf("too many lights, you should increase MAX_LIGHTS!\n");
        exit(0);
      }
      lights[num_lights++] = l;
    }
    else
    {
      printf("unknown type in scene description:\n%s\n",type);
      exit(0);
    }
  }
  return 0;
}

void display()
{
}

void init()
{
  glMatrixMode(GL_PROJECTION);
  glOrtho(0,WIDTH,0,HEIGHT,1,-1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glClearColor(0,0,0,0);
  glClear(GL_COLOR_BUFFER_BIT);
}

void idle()
{
  //hack to make it only draw once
  static int once=0;
  if(!once)
  {
    draw_scene();
    if(mode == MODE_JPEG)
      save_jpg();
  }
  once=1;
}

int main(int argc, char ** argv)
{
  if ((argc < 2) || (argc > 3))
  {  
    printf ("Usage: %s <input scenefile> [output jpegname]\n", argv[0]);
    exit(0);
  }
  if(argc == 3)
  {
    mode = MODE_JPEG;
    filename = argv[2];
  }
  else if(argc == 2)
    mode = MODE_DISPLAY;

  glutInit(&argc,argv);
  loadScene(argv[1]);

  glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
  glutInitWindowPosition(0,0);
  glutInitWindowSize(WIDTH,HEIGHT);
  int window = glutCreateWindow("Ray Tracer");
  glutDisplayFunc(display);
  glutIdleFunc(idle);
  init();
  glutMainLoop();
}

